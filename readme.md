# WP Media Bins

WP Media Bins is a simple WordPress plugin which groups media files and eventually other objects into "bins". These bins can then be used to do things like assign properties to the bin with ACF and use throughout a theme or plugin.
