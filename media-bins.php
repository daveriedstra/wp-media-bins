<?php
/**
 * Plugin Name:			Media Bins
 * Plugin URI:			https://gitlab.com/daveriedstra
 * Description:			Creates bins to group ACF attributes for media files.
 * Version:				0.1.0
 * Author:				Dave Riedstra
 * Text Domain:			dried-media-bins
 * License:				GPL v2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

function media_bins_register_post_types() {
	register_post_type('dried_visual_bins', [
		'labels' => [
			'name'			=> __('Visual Bins', 'dried-visual-bins'),
			'singular_name' => __('Visual Bin', 'dried-visual-bins')
		],
		'public'	=> true,
        'publicly_queryable' => true,
        'show_in_rest' => true,
        'rest_base' => 'visual-bins',
		'rewrite'	=> ['slug' => 'visual-bin'],
		'supports' => [ 'title', 'revisions', 'custom-fields' ]
	]);

	register_post_type('dried_audio_bins', [
		'labels' => [
			'name'			=> __('Audio Bins', 'dried-audio-bins'),
			'singular_name' => __('Audio Bin', 'dried-audio-bins')
		],
		'public'	=> true,
        'publicly_queryable' => true,
        'show_in_rest' => true,
        'rest_base' => 'audio-bins',
		'rewrite'	=> ['slug' => 'audio-bin'],
		'supports' => [ 'title', 'revisions', 'custom-fields' ]
	]);

	register_post_type('dried_pools', [
		'labels' => [
			'name'			=> __('Pools', 'dried-media-bins'),
			'singular_name' => __('Pool', 'dried-media-bins')
		],
		'public'	=> true,
        'publicly_queryable' => true,
        'show_in_rest' => true,
        'rest_base' => 'pools',
		'rewrite'	=> ['slug' => 'pool'],
		'supports' => [ 'title', 'revisions', 'custom-fields' ]
	]);
}
add_action('init', 'media_bins_register_post_types');

function media_bins_activate_plugin() {
	media_bins_register_post_types();
	flush_rewrite_rules();
}
register_activation_hook(__FILE__, 'media_bins_activate_plugin');

function media_bins_deactivate_plugin() {
	unregsiter_post_type('dried_media_bins');
	flush_rewrite_rules();
}
register_deactivation_hook(__FILE__, 'media_bins_deactivate_plugin');

/**
 * hook for ACF save action.
 * used to call function for two-way sync posts and bins field relations.
 */
function media_bins_save_acf_post($post_id) {
  media_bins_sync_files_bins($post_id);
}
/* add_action('acf/save_post', 'media_bins_save_acf_post', 5); */

/**
 * Ensures that file and bin relations are kept in sync when updated.
 *
 * First, finds additions (entries in new_vals not present in prev_vals) and
 * ensures current post is present there.
 *
 * Then finds removals (entries in prev_vals not present in new_vals) and
 * ensures current post is NOT present there.
 *
 * This approach should work regardless of whether an audio file or a bin is
 * being changed.
 */
function media_bins_sync_files_bins($post_id) {
	// Is the current post a bin or a file?
	$is_bin = true;
	$prev_vals = [];
	$new_vals = [];

	// bail early if no ACF data
	if (!isset($_POST['acf']))
		return;

	$post_type = get_post_type($post_id);
	$is_bin = $post_type == 'dried_media_bins';

	if ($is_bin) {
		$prev_vals = get_field('audio', $post_id);
		$new_vals = isset($_POST['acf']['dmb_files']) ? $_POST['acf']['dmb_files'] : [];
	} else {
		$prev_vals = get_field('bins', $post_id);
		$new_vals = isset($_POST['acf']['dmb_bins']) ? $_POST['acf']['dmb_bins'] : [];
	}

    if (!is_array($prev_vals))
        $prev_vals = [];

    if (!is_array($new_vals))
        $new_vals = [];

    $field = $is_bin ? 'bins' : 'files';

    // @TODO - fix array_diff to work with WP_Post
	$additions = array_diff($new_vals, $prev_vals);
	foreach ($additions as $added_post_id) {
        // addition, add post to relative [field]
        $file_bins = get_field($field, $added_post_id);
        if (!is_array($file_bins))
            $file_bins = [];
        array_push($file_bins, $post_id);
        update_field($field, $file_bins, $added_post_id);
	}

	$removals = array_diff($prev_vals, $new_vals);
	foreach ($removals as $removed_post_id) {
        // remove post from relative [field]
        $file_bins = get_field($field, $removed_post_id);
        if (!is_array($file_bins))
            $file_bins = [];
        $idx = array_search($post_id, $file_bins);
        $file_bins = array_diff($file_bins, [$post_id]);
        update_field($field, $file_bins, $removed_post_id);
	}
}
